#include <Arduino.h>
#include <NeoPixelBus.h>
#include <SimpleDHT.h>
#include "SSD1306.h"

SimpleDHT11 dht11;
SSD1306 display(0x3c, D2, D1);
NeoPixelBus<NeoRgbFeature, NeoEsp8266BitBang400KbpsMethod> strip(3, 2);

RgbColor ampelphasen[][3] = {
    {RgbColor(255, 0, 0), RgbColor(0, 0, 0), RgbColor(0, 0, 0)},
    {RgbColor(255, 0, 0), RgbColor(255, 255, 0), RgbColor(0, 0, 0)},
    {RgbColor(0, 0, 0), RgbColor(0, 0, 0), RgbColor(0, 255, 0)},
    {RgbColor(0, 0, 0), RgbColor(255, 255, 0), RgbColor(0, 0, 0)}};

int aktuell = 0;

void setup()
{
    pinMode(0, INPUT_PULLUP);
    strip.ClearTo(RgbColor(0, 0, 0));
    strip.Show();

    display.init();
    display.flipScreenVertically();
    display.setFont(ArialMT_Plain_10);
}

void loop()
{
    display.clear();

    for (int i = 0; i < 3; i++)
    {
        strip.SetPixelColor(2 - i, ampelphasen[aktuell][i]);
    }

    static int isPressed = 0;
    if (!digitalRead(0) != isPressed)
    {
        isPressed = !digitalRead(0);
        if (isPressed)
        {
            aktuell = ++aktuell % 4;
        }
    }

    strip.Show();

    display.display();
}