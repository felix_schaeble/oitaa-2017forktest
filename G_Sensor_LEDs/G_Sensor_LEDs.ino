// WiFi Zugangsdaten in Zeile 203 eintragen

#include <Arduino.h>
#include <NeoPixelBus.h>
#include <Hash.h>

#include "SSD1306.h" // display
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <WebSocketsServer.h>

NeoPixelBus<NeoRgbFeature, NeoEsp8266BitBang400KbpsMethod> strip(3, 2);

SSD1306 display(0x3c, D2, D1);
ESP8266WebServer http(80);
WebSocketsServer websocket(81);

void changeLeds(RgbColor color){
  Serial.println((String)color.R + ", " + (String)color.G + ", " + (String)color.B);
  strip.ShiftRight(1);
  strip.SetPixelColor(0, color);
  strip.Show();
}

void indexHtml()
{
  http.send(200, "text/html", R"=====(<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>device orientation</title>
</head>

<body>
  <h1>Color:
    <span id="color"></span>
  </h1>
  <h1>Alpha:
    <span id="alpha"></span>
  </h1>
  <h1>Beta:
    <span id="beta"></span>
  </h1>
  <h1>Gamma:
    <span id="gamma"></span>
  </h1>



  <script>

    var connection = new WebSocket('ws://' + location.hostname + ':81');
    var color = [0, 0, 0];
    
    connection.onopen = function(event){
      window.addEventListener('deviceorientation', orientation_callback, true);
      // Senden der Farbe nur 10 mal pro Sekunde
      window.setInterval(send, 100);
    }
    
    connection.onerror = function(event){
      alert(event.message)
    }
    
    function send(){
      console.log('send', color);
      if(connection.readyState === 1)
        connection.send(color);
    }

    function orientation_callback(event) {

      // Absolutes (Erd-) Koordinatensystem
      var absolute = event.absolute;

      // z-Achse (nach oben) 0 bis 360
      var alpha = event.alpha;
      // x-Achse (nach rechts) -180 bis 180
      var beta = event.beta;
      // y-Achse (nach vorn) -90 bis 90
      var gamma = event.gamma;

      if(!beta && beta !== 0){
        document.querySelector("#color").innerHTML = "No sensor data!";
        return;
      }

      beta = Math.min(beta, 90);
      beta = Math.max(beta, 0);
      beta = beta / 90;

      gamma += 90;
      gamma = Math.min(gamma, 180);
      gamma = Math.max(gamma, 0);
      gamma = gamma / 180;
      
      color = hslToRgbHex(gamma, 1, beta);
      document.querySelector("#color").innerHTML = color;      
      document.querySelector("body").style.background =
        '#' + color;
    }
    function hslToRgbHex(h, s, l){
      rgb = hslToRgb(h, s, l);
      return rgbToHex(rgb[0], rgb[1], rgb[2])
    }
    /**
    * Converts an HSL color value to RGB. Conversion formula
    * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
    * Assumes h, s, and l are contained in the set [0, 1] and
    * returns r, g, and b in the set [0, 255].
    *
    * @param   Number  h       The hue
    * @param   Number  s       The saturation
    * @param   Number  l       The lightness
    * @return  Array           The RGB representation
    */
    function hslToRgb(h, s, l) {
      var r, g, b;

      if (s == 0) {
        r = g = b = l; // achromatic
      } else {
        function hue2rgb(p, q, t) {
          if (t < 0) t += 1;
          if (t > 1) t -= 1;
          if (t < 1 / 6) return p + (q - p) * 6 * t;
          if (t < 1 / 2) return q;
          if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
          return p;
        }

        var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        var p = 2 * l - q;

        r = hue2rgb(p, q, h + 1 / 3);
        g = hue2rgb(p, q, h);
        b = hue2rgb(p, q, h - 1 / 3);
      }

      return [
        Math.round(r * 255),
        Math.round(g * 255),
        Math.round(b * 255)
      ];
    }
    function rgbToHex(red, green, blue) {
        var rgb = blue | (green << 8) | (red << 16);
        return (0x1000000 + rgb).toString(16).slice(1)
    }  
  </script>
</body>

</html>)=====");
}

String getReadableIp(IPAddress ip)
{
  return (String)ip[0] + "." + (String)ip[1] + "." + (String)ip[2] + "." + (String)ip[3];
}

void onText(uint8_t *payload, size_t length)
{
  char buffer[length];
  sprintf(buffer, "%s", payload);
  long number = strtol(buffer, NULL, 16);
  RgbColor rgb(
      number >> 16,
      number >> 8 & 0xFF,
      number & 0xFF);
  changeLeds(rgb);
}

void websocketEvent(uint8_t num, WStype_t type, uint8_t *payload, size_t length)
{
  switch (type)
  {
  case WStype_DISCONNECTED:
    return;
  case WStype_CONNECTED:
    Serial.println("Websocket connected");
    return;
  case WStype_TEXT:
    onText(payload, length);
    return;
  case WStype_BIN:
    return;
  }
}

void setup()
{
  Serial.begin(115200);

  strip.ClearTo(RgbColor(0, 0, 0));
  strip.Show();

  display.init();
  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);

  WiFi.mode(WIFI_STA);
  WiFi.begin("***", "***");
  while (WiFi.waitForConnectResult() != WL_CONNECTED)
  {
    Serial.println("Connection Failed! Rebooting...");
    delay(1000);
    ESP.restart();
  }

  display.drawString(0, 0, getReadableIp(WiFi.localIP()));
  display.display();

  http.on("/", indexHtml);

  http.begin();

  websocket.onEvent(websocketEvent);

  websocket.begin();
  Serial.println("Started");
}

void loop()
{
  http.handleClient();
  websocket.loop();
}
