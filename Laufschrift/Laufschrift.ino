#include <ESP8266WebServer.h>
ESP8266WebServer http(80);

#include <NeoPixelBus.h>
NeoPixelBus<NeoRgbFeature, NeoEsp8266BitBang400KbpsMethod> strip(256, 1);

#include <Ticker.h>
Ticker ticker;

#include "wifi_setup.h"
void wifi_setup();
String wifi_info();

#include "index_html.hpp"
String index_html(String text, String color);

#include "zeichensatz.hpp"
extern const unsigned char zeichensatz8x5_definition[96][5];

const int textmaxlength = 100;

int speed = 1; //Hz
String text;
byte textBytes[textmaxlength * 6 + 32];
int bytesLength = 0;
RgbColor colors[] = {
    RgbColor(0, 0, 0),
    RgbColor(0, 0, 255)};

void matrix()
{
    static int position = -32;
    int x, y, on;

    for (int i = 0; i < 256; i++)
    {
        y = i / 32;
        x = ((y % 2 == 0) ? i % 32 : 31 - i % 32) + position;
        on = y < position && ((textBytes[x] << y) & 128) != 0;
        strip.SetPixelColor(i, colors[on]);
    }

    strip.Show();

    position = (position != bytesLength) ? position + 1 : -32;
}

void setBits()
{    
    int p = 32;
    // Text in Bytearray schreiben
    for (int x = 0; x < text.length(); x++){
        textBytes[p++] = 0;
        for (int y = 0; y < 5; y++, p++)
            textBytes[p] = (byte)zeichensatz8x5_definition[text.charAt(x) - ' '][y];
    }
    bytesLength = p;

    // Restliches Array mit Leerzeichen füllen
    for (; p < textmaxlength * 6 + 32; p++)
            textBytes[p] = (byte)0;
}

RgbColor toRgbColor(String color)
{
    if (color.charAt(0) == '#')
        color = color.substring(1);
    long c = (long)strtol(color.c_str(), NULL, 16);
    return RgbColor(
        (c & 0xFF0000) >> 16,
        (c & 0x00FF00) >> 8,
        (c & 0x0000FF));
}

String fromRgbColor(RgbColor color)
{
    long c = ((color.R & 0x0ff) << 16) | ((color.G & 0x0ff) << 8) | (color.B & 0x0ff);
    int i = 6;
    char buffer[8];
    while (i > 0)
    {
        buffer[i--] = "0123456789ABCDEF"[c % 16];
        c /= 16;
    }
    buffer[0] = '#';
    buffer[7] = '\0';
    return String(buffer);
}

void setText(String t)
{
    text = t;
    setBits();
    Serial.println("set text to: '" + t + "'");
}

void setColor(String c)
{
    colors[1] = toRgbColor(c);
    Serial.println("set color to: " + fromRgbColor(colors[1]));
}

void setSpeed(int f)
{
    ticker_stop();
    speed = f;
    Serial.println("set speed to: '" + (String)speed + "'");
    ticker_start();
}

void ticker_start()
{
    ticker.attach(1.0f / speed, matrix);
}

void ticker_stop()
{
    ticker.detach();
}

void webserver_setup()
{

    http.on("/", []() {
        http.send(200, "text/html", index_html(text, fromRgbColor(colors[1]), speed));
    });

    http.on("/setcolor", []() {
        setColor(http.arg("color"));
        http.send(200, "application/json", "");
    });

    http.on("/settext", []() {
        setText(http.arg("text").substring(0, textmaxlength - 1));
        http.send(200, "application/json", "");
    });

    http.on("/setspeed", []() {
        setSpeed(http.arg("speed").toInt());
        http.send(200, "application/json", "");
    });

    http.begin();
}

void setup()
{
    // Für Verwendung in der Laufschrift deaktivieren. Laufschrift LED-Kette verwendet TXD!
    // Serial.begin(9600);

    strip.ClearTo(RgbColor(0, 0, 0));
    strip.Show();

    wifi_setup([](int c) {
        for (int i = 0; i < c; i++)
            strip.SetPixelColor(i, colors[1]);
        strip.Show();
    });

    webserver_setup();

    //setText("Willkommen im IoT-Lab der Hochschule Aalen (Fak. Elektronik und Informatik)");
    setText(wifi_info());
    setColor("#00FF00");
    setSpeed(10);
}

void loop()
{
    http.handleClient();
}
