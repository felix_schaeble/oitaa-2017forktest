#include <Arduino.h>
#include <ESP8266WiFi.h>

 #define WIFI_SSID "WIFI_SSID"
 #define WIFI_PASS "WIFI_PASS"

extern void wifi_setup(const std::function<void(int)>& progressCallback = {})
{
    WiFi.mode(WIFI_STA);
    Serial.printf("[WIFI] connection to %s", WIFI_SSID);
    WiFi.begin(WIFI_SSID, WIFI_PASS);
    int cycle = 0;
    while (WiFi.status() != WL_CONNECTED)
    {
        Serial.print('.');
        if(progressCallback)
            progressCallback(cycle++);
        delay(100);
    }
    Serial.println();
    Serial.printf("[WiFi] STATION Mode, SSID: %s, IP address: %s\n", WiFi.SSID().c_str(), WiFi.localIP().toString().c_str());
}

extern String wifi_info(){
    if(WiFi.status() == WL_CONNECTED)
        return WiFi.localIP().toString();
    else
        return "Not connected";
}
