#include "SSD1306.h"
#include <SimpleDHT.h>

const int dreh1 = 12, dreh2 = 13, spk = 15, push = 0, dhtpin = 14, ledpin = 16;

SimpleDHT11 dht11;

SSD1306  display(0x3c, D2, D1);
#include <NeoPixelBus.h>

const int n = 3;
NeoPixelBus<NeoRgbFeature, NeoEsp8266BitBang400KbpsMethod> strip(n, 2);

void setup() {
  display.init();
  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);

  pinMode(spk, OUTPUT);
  pinMode(push, INPUT_PULLUP);
  pinMode(dreh1, INPUT_PULLUP);
  pinMode(dreh2, INPUT_PULLUP);
  pinMode(ledpin, OUTPUT);

}

void loop() {
  static uint32_t t = millis();
  int dreh = 2 * digitalRead(dreh1) + digitalRead(dreh2);
  static int i1, i2;

  static byte temperature = 0, humidity = 0;
  
  display.clear();
  display.drawString(0, 0,  "Drehgeber :     "  + (String) dreh);
  display.drawString(0, 16, "Analog    :     "  + (String) analogRead(A0) + "   ");

  if (millis() - t >= 1000) { 
    dht11.read(dhtpin, &temperature, &humidity, NULL);
    t += 1000;
    digitalWrite(ledpin, !digitalRead(ledpin));
    if (!digitalRead(push))
      tone(spk, 440, 500);
    i2 = (i1 + 1) % n;
    strip.SetPixelColor(i2, RgbColor(0, 0, 200));
    strip.SetPixelColor(i1, RgbColor(0x00, 0x00, 0x00));
    strip.Show();
    i1 = i2;
  }
  display.drawString(0, 32, "Umwelt    :  " + (String) temperature + "°" + " " + (String) humidity + "\%   ");

  display.drawString(0, 48, "Zum Piepsen links drücken!");

  display.display();
}
