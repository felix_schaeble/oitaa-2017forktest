/*
Rechnerle Alexa

Benötigte Bibliotheken:
    - ESPAsyncTCP by Hristo Gochkov v1.1.3
      https://github.com/me-no-dev/ESPAsyncTCP/archive/master.zip
    - FauxmoESP by Xose Perez 2.4.2
      https://bitbucket.org/xoseperez/fauxmoesp/downloads
*/

#include <Arduino.h>
#include <fauxmoESP.h>
#include <NeoPixelBus.h>
#include "wifi_setup.h"

String wifi_setup(String, String);

#define WIFI_SSID "IOT-LAB-HS-AALEN"
#define WIFI_PASS "oitaa2017"

NeoPixelBus<NeoRgbFeature, NeoEsp8266BitBang400KbpsMethod> strip(3, 2);
fauxmoESP fauxmo;

void onSetState(unsigned char device_id, const char *device_name, bool state)
{
    Serial.printf("[MAIN] Device #%d (%s) state: %s\n", device_id, device_name, state ? "ON" : "OFF");
    int i = state ? 255 : 0;
    strip.ClearTo(RgbColor(i, i, i));
    strip.Show();
}

bool onGetState(unsigned char device_id, const char *device_name)
{
    return true;
}

void setup()
{
    Serial.begin(115200);

    strip.ClearTo(RgbColor(0, 0, 0));
    strip.Show();

    wifi_setup(WIFI_SSID, WIFI_PASS);

    fauxmo.addDevice("Kühlschrank");
    fauxmo.enable(true);

    fauxmo.onSetState(onSetState);
    fauxmo.onGetState(onGetState);
}

void loop()
{
    fauxmo.handle();
}
