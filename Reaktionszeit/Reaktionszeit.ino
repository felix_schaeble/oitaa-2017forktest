#include <NeoPixelBus.h>
#include <SimpleDHT.h>
#include "SSD1306.h"

SimpleDHT11 dht11;
SSD1306 display(0x3c, D2, D1);
NeoPixelBus<NeoRgbFeature, NeoEsp8266BitBang400KbpsMethod> strip(3, 2);

bool led_on = false;
long last = 0;
long delta = 0;
long current = 0;


void setReactionDelay()
{
    delta = rand() % 9000 + 1000; // delay is between 1 and 10 seconds
    last = millis();
    Serial.println("Set delay to: " + (String) delta);
}

void waitForSwitchLed()
{
    if (millis() - last < delta)
        return;

    strip.SetPixelColor(1, RgbColor(255, 255, 255));
    led_on = true;
    delta = 0;
    last = millis();
}

void waitForReaction()
{
    display.drawString(0, 0, (String)((millis() - last) / 1000.0f));
    if (!digitalRead(0))
    {
        delta = millis() - last;
    }
}

void waitForReset()
{
    display.drawString(0, 0, (String)(delta / 1000.0f));
    if(digitalRead(0)){
        strip.ClearTo(RgbColor(0, 0, 0));
        delta = 0;
        led_on = false;
    }
}

void setup()
{
    pinMode(0, INPUT_PULLUP);

    Serial.begin(115200);

    strip.ClearTo(RgbColor(0, 0, 0));
    strip.Show();

    display.init();
    display.flipScreenVertically();
    display.setFont(ArialMT_Plain_10);

    display.drawString(0,0,"wait...");
    display.display();
    delay(5000);
}

void loop()
{
    display.clear();

    if (!led_on && !delta)
        setReactionDelay();
    else if (!led_on && delta)
        waitForSwitchLed();
    else if (led_on && !delta)
        waitForReaction();
    else
        waitForReset();

    display.display();
    strip.Show();
}
